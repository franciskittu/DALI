DALI Examples
=============


The examples are divided into two subfolders:
* _basic_ : aimed at basic Windows-based setup, no agent types, every agent living in a separated sicstus window.
* _advanced_ : more complex, aimed at Unix-like based environment, with agent type, instances, each agent living in a separated xterm console
